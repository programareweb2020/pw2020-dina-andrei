const express = require('express');
const helmet = require('helmet');

const routes = require('./routes');

const app = express();

app.use(helmet());
app.use(express.json());

app.use('/api/v1', routes);

app.use((err, req, res, next) => {
    console.trace(err);

    let status = 500;
    let message = 'Something Bad Happened';
    if (err.httpStatus) {
        status = err.httpStatus;
        message = err.message;
    }
    res.status(status).json({
        error: message,
    });
});

app.listen(3000, () => {
    console.log('Serverul a pornit pe portul 3000');
});