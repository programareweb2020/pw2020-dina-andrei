const database = require('../data/database.js');
const {
    ServerError
} = require('../errors');

const add = (payload) => {
    database.insertIntoDb(payload);
}

const getAll = () => {
    return database.getAllFromDb();
}

const getById = (id) => {
    try {
        return database.getFromDbById(id);
    } catch (e) {
        if (e.message === `The object with the id = ${id} does not exists!`) {
            throw new ServerError(e.message, 400);
        }
        throw e;
    }
}

const getByAuthor = (author) => {
    return database.getFromDbByAuthor(author);
}

const updateById = (id, payload) => {
    try {
        database.updateById(id, payload);
    } catch (e) {
        if (e.message === `The object with the id = ${id} does not exists!`) {
            throw new ServerError(e.message, 400);
        }
        throw e;
    }
}

const deleteById = (id) => {
    database.removeFromDbById(id);
}

const deleteByAuthor = (author) => {
    database.removeFromDbByAuthor(author);
}

const deleteAll = () => {
    database.purgeDb();
}

module.exports = {
    add,
    getAll,
    getById,
    getByAuthor,
    updateById,
    deleteAll,
    deleteByAuthor,
    deleteById
}