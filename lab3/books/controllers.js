const express = require('express');

const BookService = require('./services.js');
const {
    validateField
} = require('../utils');

const Router = express.Router();

// CREATE
Router.post('/', (req, res) => {
    const {
        title,
        author
    } = req.body;

    validateField(title, 'ascii');
    validateField(author, 'ascii');

    BookService.add({ title, author });
    res.status(201).send("Resursa adaugata cu succes!");
});

// READ
Router.get('/', (req, res) => {

    if (req.query.author) {
        const booksByAuthor = BookService.getByAuthor(req.query.author);
        res.status(200).json(booksByAuthor);
    }

    const books = BookService.getAll();
    res.json(books);
});

Router.get('/:id', (req, res) => {

    validateField(req.params.id, 'int');

    const id = parseInt(req.params.id);

    const book = BookService.getById(id);
    res.status(200).send(book);
});

// UPDATE
Router.put('/:id', (req, res) => {

    const body = req.body;
    validateField(req.params.id, 'int');

    const id = parseInt(req.params.id);

    BookService.updateById(id, body);
    res.status(200).send(`Book with ${id} updated succesfully!`);
});

// DELETE
Router.delete('/:id', (req, res) => {
    
    validateField(req.params.id, 'int');
    const id = parseInt(req.params.id);
    BookService.deleteById(id);
    res.status(200).send("Resursa stearsa cu succes!");
});

Router.delete('/', (req, res) => {
    if (req.query.author) {
        validateField(req.query.author, 'ascii');
        BookService.deleteByAuthor(req.query.author);
        res.status(200).send(`Cartile cu autorul ${req.query.author} au fost sterse cu succes!`);
    }

    BookService.deleteAll();
    res.status(200).send("Baza de date a fost golita cu succes!");
});

module.exports = Router;