const express = require('express');
const BookControllers = require('../books/controllers.js');

const router = express();

router.use('/books', BookControllers);

module.exports = router;