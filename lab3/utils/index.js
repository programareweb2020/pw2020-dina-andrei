const validator = require('validator');

const {
    ServerError
} = require('../errors');
/**
 * 
 * @param {*} field 
 * @param {String} type 
 * @throws {ServerError}
 */
const validateField = (field, type) => {
    if (!field) {
        throw new ServerError(`Lipseste campul ${field}`, 400);
    }

    switch (type) {
        case 'ascii':
            if (!validator.isAscii(field)) {
                throw new ServerError(`Campul ${field} trebuie sa contina doar caractere ascii`, 400);
            }
            break;
        case 'alpha':
            if (!validator.isAlpha(field)) {
                throw new ServerError(`Campul ${field} trebuie sa contina doar litere`, 400);
            }
            break;
        case 'int':
            if (!validator.isInt(field)) {
                throw new ServerError(`Campul ${field} trebuie sa fie un numar intreg`, 400);
            }
    }
}

module.exports ={
    validateField
}