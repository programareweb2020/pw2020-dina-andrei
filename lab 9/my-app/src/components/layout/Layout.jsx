import React from 'react';
import Header from './Header';
import Nav from './Nav';
import Footer from './Footer';

const Layout = ({children}) => {

    return(
        <div className="layout">
            <Header/>
            <Nav/>
            <div className="content">
                {children}
            </div>
            <Footer/>
        </div>
    )
}

export default Layout;