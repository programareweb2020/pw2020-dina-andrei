import React from 'react';
import './App.scss';
import Layout from './components/layout'
import Count from './components/counter';

function App() {
  return (
    <div className="App">
        <Layout>
            <Count/>
        </Layout>
    </div>
  );
}

export default App;
