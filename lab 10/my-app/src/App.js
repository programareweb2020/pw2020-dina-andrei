import React from 'react';
import './App.scss';
import Layout from './components/layout'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";
  
import AuthorList, {Author} from './pages/authors';
import BookList, {Book} from './pages/books';
import Authentication from './pages/authentication';


function App() {
  return (
    <div className="App">
        <Router>
            <Layout>
                <nav>
                    <ul>
                        <li>
                            <Link to="/login">Login</Link>
                        </li>
                        <li>
                            <Link to="/Authors">Authors</Link>
                        </li>
                        <li>
                            <Link to="/Books">Books</Link>
                        </li>
                    </ul>
                </nav>
                <Switch>
                    <Route path="/login" component={Authentication}/>
                    <Route path="/Authors/:id" component={Author}/>
                    <Route path="/Authors" component={AuthorList}/>
                    <Route path="/Boooks/:id" component={Book}/>
                    <Route path="/Books" component={BookList}/>
                </Switch>
            </Layout>
        </Router>
    </div>
  );
}

export default App;
