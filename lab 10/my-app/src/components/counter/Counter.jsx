import React, { useState, useEffect } from 'react';

const Counter = () => {
    const [count, setCount] = useState(1)

    useEffect(()=>{
        if(count === 0){
            alert("Counter is 0!");
        }

    }, [count])

    const changeCount = (value) => () => {
        setCount(count + value);
    }

    return(
        <div>
            <div>Your count is: {count}</div>
            <button onClick={changeCount(1)}>Increment</button>
            <button onClick={changeCount(-1)}>Decrement</button>
            <button onClick={changeCount((-1) * count)}>Reset</button>
        </div>
    )
}

export default Counter;