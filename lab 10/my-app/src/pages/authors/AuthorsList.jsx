import React, { useState, useEffect } from 'react';
import {Link} from 'react-router-dom';
import axios from 'axios';

const AuthorsList = () => {

    const [authorList, setAuthorList] = useState([]);

    useEffect(() => {
        axios.get("http://localhost:3000/api/v1/authors")
        .then(response => response.json())
        .then(res => setAuthorList(res))
    },[])

    return (
        <div>
            {authorList.map(x => <Link to={`/Authors/${x.id}`}/>)}
        </div>
    )
}

export default AuthorsList