import React, { useState } from 'react';
import axios from 'axios';

async function postFunc(url = '', data = {}) {
    // Default options are marked with *
    const response = await fetch(url, {
        method: 'POST',
        mode: 'cors',
        cache: 'no-cache',
        credentials: 'include',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    });
    return response.json();
}

const postData = (url, data) => postFunc(url, data)

const Authentication = () => {

    const [password, setPassword] = useState('');
    const [username, setUsername] = useState('');

    const handleInputChange = (type) => (event) => {
        const value = event.target.value;
        if (type === 'password') {
            setPassword(value)
        } else if (type === "username") {
            setUsername(value);
        }
    }

    const handleRegister = () => {
        postData("http://localhost:3000/api/v1/users/register",
            { username: username, password: password })
        .then(
            response => console.log(response.status)
        )
    }

    const handleLogin = () => {
        postData("http://localhost:3000/api/v1/users/login",
            { username: username, password: password })
        .then(
            response => {
                console.log(response)
                localStorage.setItem("token", response.token);
            }
        )
    }

    return (
        <div>
            <input value={username} onChange={handleInputChange("username")} />
            <input value={password} type="password" onChange={handleInputChange("password")} />
            <button onClick={handleRegister}>Register</button>
            <button onClick={handleLogin}>Login</button>
        </div>
    )
}

export default Authentication;