import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

const BookList = () => {

    const [bookList, setBookList] = useState([]);

    useEffect(() => {
        axios.get("http://localhost:3000/api/v1/books")
            .then(response => response.json())
            .then(res => setBookList(res))
    }, [])

    return (
        <div>
            {bookList.map(x => <Link to={`/Books/${x.id}`} />)}
        </div>
    )
}

export default BookList