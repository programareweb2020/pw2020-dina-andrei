const {
    Books
} = require('../data');

const add = async (name, authorId, genres) => {
    // create new Book obj
    // save it
    const book = new Books({
        name,
        author: authorId,
        genres
    });
    await book.save();
};

const getAll = async () => {
    // get all books
    // populate 'author' field
    // modify output so author is made of 'author.firstName author.lastName'
    const value = await Books.find().populate('author', "firstName lastName -_id");
    return value;
};

const getById = async (id) => {
    // get book by id
    // populate 'author' field
    // modify output so author is made of 'author.firstName author.lastName'
    const value = await Books.findById(id).populate('author', "firstName lastName -_id");
    return value;

};

const getByAuthorId = async (id) => {
    // get book by author id
    // modify output so author is made of 'author.firstName author.lastName'
    const value = await Books.find({"author":id}).populate('author', "firstName lastName -_id");
    return value;
};

const updateById = async (id, name, authorId, genres) => {
    // update by id
    await Books.findByIdAndUpdate(id, {
        name,
        author: authorId,
        genres
    });
};

const deleteById = async (id) => {
    // delete by id
    await Books.findByIdAndDelete(id);
};

module.exports = {
    add,
    getAll,
    getById,
    getByAuthorId,
    updateById,
    deleteById
}