const express = require('express');

const PublisherService = require('./services.js');
const { validateFields } = require('../utils');
const { ServerError } = require('../errors');

const router = express.Router();

router.get('/', async (req, res, next) => {

    try {
        const publishers = await PublisherService.getAll();
        const only_names = publishers.map(item => item.name);

        res.json(only_names);

    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js 
        next(err);
    }
});

router.get('/:id', async (req, res, next) => {
    const { id } = req.params;

    try {
        validateFields({
            id: {
                value: id,
                type: 'int'
            }
        });

        const publisher = await PublisherService.getById(parseInt(id));
    
        res.json(publisher.name);

    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js 
        next(err);
    }
});

router.post('/', async (req, res, next) => {
    const { name } = req.body;

    // validare de campuri
    try {
        const fieldsToBeValidated = {
            name: {
                value: name,
                type: 'alpha'
            }
        };

        validateFields(fieldsToBeValidated);

        await PublisherService.add(name);

        res.status(201).end();

    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js 
        next(err);
    }
});

router.post('/pub_book', async (req, res, next) => {
    const { book_id, publisher_id, price } = req.body;

    // validare de campuri
    try {
        const fieldsToBeValidated = {
            book_id: {
                value: book_id,
                type: 'int'
            },
            publisher_id: {
                value: publisher_id,
                type: 'int'
            },
            price: {
                value: price,
                type: 'int'
            },
        };

        validateFields(fieldsToBeValidated);

        await PublisherService.addPB(parseInt(book_id),parseInt(publisher_id),parseInt(price));


        res.status(201).end();

    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js 
        next(err);
    }
});

router.put('/:id', async (req, res, next) => {
    const { id } = req.params;
    const { name } = req.body;

    try {
        const fieldsToBeValidated = {
            id: {
                value: id,
                type: 'int'
            },
            name: {
                value: name,
                type: 'alpha'
            }
        };

        validateFields(fieldsToBeValidated);

        await PublisherService.updateById(parseInt(id), first_name, last_name);

        res.status(204).end();

    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js 
        next(err);
    }
});

router.delete('/:id', async (req, res, next) => {
    const { id } = req.params;

    try {
        validateFields({
            id: {
                value: id,
                type: 'int'
            }
        });
        
        await PublisherService.deleteById(parseInt(id));

        res.status(204).end();

    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js 
        next(err);
    }
});

module.exports = router;