const express = require('express');

const BooksService = require('./services.js');
const AuthorService = require('../Authors/services');
const { validateFields } = require('../utils');
const { ServerError } = require('../errors');

const router = express.Router();

router.get('/', async (req, res, next) => {

    try {
        const books = await BooksService.getAll();
        const authors = await AuthorService.getAll();
        

        const books_join = books.map(book => {
            const author = authors.find(el => el.id === book.author_id);
            if(author){
                return {
                    ...book,
                    first_name: author.first_name, 
                    last_name: author.last_name
                }
            } else {
                return {...book};
            }
        });

        res.json(books_join);

    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js 
        next(err);
    }
});

router.get('/:id', async (req, res, next) => {
    const { id } = req.params;

    try {
        validateFields({
            id: {
                value: id,
                type: 'int'
            }
        });

        const book = await BooksService.getById(parseInt(id));

        res.json(book);

    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js 
        next(err);
    }
});

router.post('/', async (req, res, next) => {
    const { name, author_id } = req.body;

    // validare de campuri
    try {
        const fieldsToBeValidated = {
            first_name: {
                value: name,
                type: 'alpha'
            },
            id: {
                value: author_id,
                type: 'int'
            }
        };

        validateFields(fieldsToBeValidated);

        await BooksService.add(name, parseInt(author_id));


        res.status(201).end();

    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js 
        next(err);
    }
});


router.put('/:id', async (req, res, next) => {
    const { id } = req.params;
    const { name, author_id } = req.body;

    try {
        const fieldsToBeValidated = {
            id: {
                value: id,
                type: 'int'
            },
            first_name: {
                value: name,
                type: 'alpha'
            },
            author_id: {
                value: author_id,
                type: 'int'
            }
        };

        validateFields(fieldsToBeValidated);

        await BooksService.updateById(parseInt(id), name, parseInt(author_id));

        res.status(204).end();

    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js 
        next(err);
    }
});

router.delete('/:id', async (req, res, next) => {
    const { id } = req.params;

    try {
        validateFields({
            id: {
                value: id,
                type: 'int'
            }
        });

        
        await BooksService.deleteById(parseInt(id));

        res.status(204).end();
        
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js 
        next(err);
    }
});

module.exports = router;